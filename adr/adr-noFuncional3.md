# BackEnd

## Estatus

Propuesta

## Context

Para el proyecto se debe elegir la mejor herramienta para nuestra estrategia de API

## Decision

Las caracteristicas que buscamos son:
- Exponer las API de forma f�cil y segura
- La plataforma API Management (APIM) debe admitir modelos de implementaci�n flexibles
- Gesti�n del dise�o y del tiempo de ejecuci�n para todo el ciclo de vida de las APIs
- La plataforma debe proporcionar una seguridad de API mejorada

Para esto vamos a escoger la mejor opci�n entre .Net Core 6 y Sprint Boot.

### Uso de RAM
![RAM](images/RAM.png)

### M�s Popular
![Pupular](images/MasUsado.PNG)

### Request por segundo
![Request](images/Request.PNG)

## Consequences

- Si la seguridad est� configurada de manera deficiente. Las implementaciones con fugas expusieron las API REST que permit�an a los atacantes descargar datos del usuario final a trav�s de solicitudes GET e incluso realizar cambios en los datos con solicitudes PUT.
- Posibles recursos limitados
