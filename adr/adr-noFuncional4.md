# Utilizar protocolos de autenticación seguros como OAuth 2.0 u OpenID Connect para la autenticación, autorización de usuarios

## Status

Propuesto

## Context
<br>

Debido a que el sitio web estará expuesto a la internet y debe permitir autenticación y autorización para acceso a componentes internos del sitio (APIS, MICROSERVICIOS, MICROFRONENDS) a colaboradores, proveedores y para usuarios que mantengan un rol activo, es necesario implementar protocolos que garanticen la seguridad del espacio personalizado para cara tipo de usuario y el acceso a los datos que este pueda alojar.

## Decisión
### Opciones al alcance
<br>

#### OpenID Connect
<br>

Es una capa de identidad sobre el protocolo  OAuth 2.0, el cual permite a los clientes verificar la identidad de un usuario basado en la autenticación realizada por un servidor de autorización

#### Oauth 2.0
<br>

Es un estándar diseñado para permitir que un sitio web o una aplicación accedan a recursos alojados por otras aplicaciones web en nombre de un usuario

### Decisión
<br>

Debido a que se plantea montar un sitio web con front end y una capa de apis back end en un ambiente on cloud, se decide utilizar Oauth 2.0 con Authorization Code Flow ya que es el mecanismo de autorización mas completo y se complementa de forma directa con componentes como un Key Vault para el guardado del Secret y se lo puede integrar a un mecanismo de autenticación nativo como un Active Directory.
<br><br>

![Como funciona OAuth 2.0](images/EsquemaOauth2.jpg)
<br><br>

## Consequences
<br>

La autorización y la autenticación son dos elementos fundamentales en la seguridad de los sistemas web, ya que juntos ayudan a garantizar que solo las personas adecuadas tengan acceso a la información y las funcionalidades que les corresponden. A continuación, explico cada uno de estos conceptos:

1. Autenticación: La autenticación se refiere al proceso de verificar la identidad de un usuario para asegurarse de que sea quien dice ser. Por lo general, esto se logra mediante credenciales como nombres de usuario y contraseñas. Sin la autenticación, cualquier persona podría ingresar al sistema web y realizar acciones en nombre de otro usuario o incluso obtener acceso no autorizado a información sensible.
La autenticación puede implementarse de diversas formas, desde credenciales básicas hasta métodos más avanzados, como autenticación de dos factores (2FA), que requiere una segunda forma de verificación, como un código enviado al teléfono del usuario.

2. Autorización: Una vez que un usuario se ha autenticado con éxito, la autorización se encarga de determinar qué acciones y recursos tiene permitido utilizar. En otras palabras, la autorización establece los niveles de acceso y los privilegios que tiene un usuario dentro del sistema.
La autorización se basa en roles y permisos. Los roles definen conjuntos predefinidos de permisos que se asignan a los usuarios, y los permisos determinan qué acciones específicas están permitidas para cada rol. Por ejemplo, un administrador del sistema puede tener acceso a todas las funcionalidades y datos, mientras que un usuario regular solo puede tener acceso a ciertas partes del sistema.

Al implementar un sistema de autorización adecuado, se garantiza que los usuarios solo puedan realizar acciones para las que tienen permiso, lo que ayuda a prevenir el acceso no autorizado y limita el potencial daño en caso de que se produzca una violación de seguridad.

En resumen, la autorización y la autenticación son importantes en un sistema web porque aseguran que solo los usuarios legítimos tengan acceso a la información y las funcionalidades adecuadas. Estos mecanismos ayudan a proteger la integridad y la confidencialidad de los datos, y evitan que personas no autorizadas obtengan acceso a recursos sensibles.