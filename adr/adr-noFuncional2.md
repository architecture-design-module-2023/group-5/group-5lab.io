# Selección de arquitectura de front-end para la aplicación móvil y web utilizando React Native

## Estatus
Propuesta

## Context
>En nuestro proyecto de desarrollo de una aplicación móvil y web para una agencia de viajes, necesitamos decidir la arquitectura de front-end que mejor se adapte a nuestros requerimientos. La aplicación debe ser multiplataforma, eficiente y ofrecer una experiencia de usuario fluida tanto en dispositivos móviles como en navegadores web. Además, se espera un alto nivel de reutilización de código y una rápida iteración en el desarrollo.

## Decision

>Después de evaluar diferentes opciones, hemos decidido utilizar una arquitectura basada en React Native para el desarrollo del front-end de la aplicación móvil y web.

>La elección de una arquitectura basada en componentes nos permitirá reutilizar código, mantener una estructura clara y facilitar la colaboración entre miembros del equipo. Al seguir las mejores prácticas de React Native, podremos aprovechar la eficiencia y las ventajas de rendimiento de esta tecnología.

## Consequences

La adopción de una arquitectura basada en React Native para el front-end de la aplicación móvil y web tiene las siguientes consecuencias:

- Requiere familiarizarse con los conceptos y patrones de desarrollo específicos de React Native, así como con las mejores prácticas para el desarrollo móvil y web.
- Implica utilizar bibliotecas y componentes específicos de React Native para lograr una interfaz de usuario nativa y de alto rendimiento en ambas plataformas.
- Permite la reutilización de gran parte del código entre la aplicación móvil y web, lo que acelera el desarrollo y simplifica el mantenimiento.
- Requiere considerar las diferencias en las interfaces de usuario y las limitaciones entre las plataformas móvil y web, y ajustar en consecuencia.
 