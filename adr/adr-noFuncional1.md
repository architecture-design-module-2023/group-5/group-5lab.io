# Base de Datos

## Status

Propuesto

## Context

Debido a que el software tendrá una gran cantidad de personas (más de 10 mil usuarios) registrados se deberá contar con una buena centralización de datos la cual garantice la confidencialidad, la integridad, la disponibilidad de la información.

## Decision

Como opciones de alcance tenemos una base de datos SQl (SQLServer) y una no SQL (MongoDB).

Se toma como decisión que la base de datos a utilizar será SQL Server con información no estrucutrada embebida, al utilizar SQL Server con información no estructurada embebida, puedes aprovechar la fortaleza y confiabilidad de una base de datos relacional para almacenar y gestionar datos estructurados, al mismo tiempo que obtienes capacidades avanzadas para trabajar con datos no estructurados. Esto te permite tener una solución más completa y escalable para el manejo de diferentes tipos de datos en tu aplicación o sistema.

Además, se plantea tener como parte de la solución una base de datos que contenga información transaccional y otras bases de datos que contengan datos analizados con el fin de tener un mejor rendimiento del software.

## Consequences

1.- Tamaño de la base de datos: El almacenamiento de datos no estructurados en la base de datos puede aumentar significativamente el tamaño de la misma, lo que puede requerir más recursos de almacenamiento.

2.- Limitaciones en el rendimiento: El procesamiento de datos no estructurados en SQL Server puede tener un impacto en el rendimiento, especialmente cuando se realizan consultas complejas o se trabaja con grandes volúmenes de datos no estructurados.

3.- Costos adicionales: El almacenamiento y el procesamiento de datos no estructurados en SQL Server pueden requerir licencias adicionales o recursos de hardware más potentes, lo que puede generar costos adicionales.
