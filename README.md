# Agencia de viajes RoadWarrior.


# Autores
- Juan Carlos Velasquez
- Stephany Contreras
- Luis Campos
- Marlon Campaña

# Descripción

Esta es la solución es para un sitio web de una importante agencia de viajes.

La aplicación web de la agencia de viajes tendrá las siguientes actividades:

- Registro de usuarios.
- Gestión de reservas de viajes.
- Alquiler de vehículos, hoteles y aerolíneas existente de la agencia.
- Información para el cliente.
- Interacción con redes sociales.
- Gestión de proveedores.
- Debe ser internacional

Las actividades en la aplicación de la agencia de viajes gira alrededor de:
- Usuarios registrados.
- Usuarios Visitantes.
- Proveedores.

Los usuarios registrados tienen las siguientes actividades:
- Inicio de sesión (login).
- Gestión de reservas (crear, ver, eliminar).
- Interacción con redes sociales.
- Ver su información en teléfonos móviles, laptos, tablets.

Los usuarios no registrados tienen las siguientes actividades:
- Navegar por el sitio de la agencia.
- Ver publicidad, promociones, etc.

Adicionalmente la solución notificará a los usuarios registrados mediante correo sobre próximos viajes.

# Visión Arquitectónica

La solución de “Road Warrior System” provista se implementará bajo los siguientes principios.
- Generar economías de escala.
- Arquitectura unificada, con lineamientos comunes, particularmente arquitecturas basadas en Microservicios de negocio.
- Diferenciación componentes Capa Front y Back.
- Soluciones basadas en componentes funcionales de fácil integración por tecnologías y estándares abiertos como REST en un modelo de arquitectura distribuida.
- Evolución de Web Services a APIs, soportados en el estándar de definiciones Open API.

Al diseñar la arquitectura, se puso un fuerte énfasis en dos características principales:
- La aplicación debe ser confiable
- La aplicación debe ser extremadamente rápida.

# Supuestos

Las siguientes suposiciones de arquitectura se basan en los impulsores y objetivos del problema comercial:
- El sistema estará alojado en Microsoft Azure con una capacidad de API para futuras integraciones.
- Se dispondrá de acceso a un Express Route para las conexiones con el Sistema existente provisto por la empresa.
- Se creará una capa de eventos para depositar la información hacia un DataLake.
- Se generará una capa de API para la integración con los sistemas internos (este será el mecanismo de integración por defecto a menos que algún factor técnico lo impida).
- El proveedor que quiera integrar sus módulos con “Road Warrior System” deberá cumplir con los lineamientos de la plataforma como mínimo debe tener una capacidad de API para enviar y obtener información sobre el estado de las accionas que ocurren en el mismo.


# Decisiones de arquitectura


La documentación de decisiones de arquitectura o Architecture Decision Records (ADR) es una colección de documentos que recogen individualmente cada una de las decisiones de arquitectura tomadas. 

|  A continuación las de esta solución |
|---|
| [Base de datos](adr/adr-noFuncional1.md)|
| [Selección de arquitectura de front-end para la aplicación móvil y web utilizando React Native](adr/adr-noFuncional2.md) |
| [BackEnd](adr/adr-noFuncional3.md) |
| [Utilizar protocolos de autenticación seguros como OAuth 2.0 u OpenID Connect para la autenticación, autorización de usuarios](adr/adr-noFuncional4.md) |


# ARQUITECTURA DE REFERENCIA

![Rererencia](adr/images/Referencia.png)

# VISION GENERAL DE LA SOLUCIÓN

![Visión General de la Solución](adr/images/VISIONGENERAL.png)

# Diagramas
Los diagramas estarán basados por el [modelo c4](https://c4model.com/)
Estos diagramas proporcionan diferentes niveles de abstracción, para un mejor entendimiento.

| [Diagramas en código](diagramas/diagrama-structurizr.dsl)|

A continuación, se detallan los diagramas para esta solución: 

- Contexto.

![Contexto](adr/images/CONTEXTO.png)

- Contenedores.

![Contenedores](adr/images/CONTENEDOR.png)
- Componentes.

![Componentes](adr/images/COMPONENTES.png)

