workspace {

    model {
    
        /* Actors */
    
        internalUser = person "Usuario Interno" "Usuario recurrente con un perfil activo."
        externalUser = person "Usuario Externo" "Usuario visitante que no cuenta con un perfil activo."
        supplier = person "Proveedor" "Usuario o sistemas que consumen necesidades específicas."
        
        /* Software Systems,containers and components */
        
        roadWarriorSoftwareSystem = softwareSystem "Road Warrior System" "Permite la interacción directa con las aerolíneas" {
            AccessManagement = container "Sistemas de identidad y manejo de accesos" "Permite garantizar la autentificación " "[Software System - Azure AD]"
            appOnePage = container "Aplicaciones de una sola página usuario interno" "Grupo de SPA que permiten la interacción de usuarios autenticados" "[Angular - MicroFrontEnd]" "Web Browser"{
                apiAuthentication = component "API - BA - AUTORIZACION" "Permite Administrar la Tokenización para autorizar el uso de componentes" "[API - BA - AUTORIZACION]"
                apiUser = component "API - BA - USUARIOS" "Permite Administrar los usuarios que inician sesión" "[Spring MVC Rest Controller]"
                apiCotization = component "API - BA - COTIZACION" "Permite Realizar cotizaciones con coordenadas de inicio y fin de viaje" "[Spring MVC Rest Controller]"
                apiReport = component "API - BA - REPORTE" "Componente Gneérico que Permite reportar vuelos, viajes, reservaciones" "[Spring MVC Rest Controller]"
                apiCampaign = component "API - BA - CAMPAÑAS" "Permite administrar camapañas de promocionales" "[Spring MVC Rest Controller]"
                apiRates = component "API - BA - TARIFAS" "Permite consultar tarifas con proveedores de servicios transversales, taxis, hoteles" "[Spring MVC Rest Controller]"
                apiContact = component "API - BA - CONTRATOS" "Permite la generación de documentos habilitantes en HTML" "[Spring MVC Rest Controller]"
                apiBooking = component "API - BA - RESERVAS" "Permite administrar reservas de viajes" "[Spring MVC Rest Controller]"
                apiBeneficiary = component "API - BA - BENEFICIARIOS" "Permite consultar si un pasajero tiene beneficiarios con impedimentos de salida del país" "[Spring MVC Rest Controller]"
                identify = component "Sistema de Identidad y Manejo de Accesos" "Permite garantizar la autentificación y autorización de accesos al sitio" "[Software System - Azure AD]"
                cacheComp = component "Caché - Parametrización - Catalogos" "Almacén de estructura de datos de valores de clave en memoria" "[PaaS - Redis Enterprise]" "Database"
                dataStructure = component "Datos Estructurados" "Aloja la información estructurada proveniente de los formularios de interaccion con el usuario" "[PaaS - Azure SQL Server]" "Database"
                dataNoStructure = component "Datos no Estructurados" "Aloja la información documental, multimedia y macrodatos que comparte el usuario con Road Warrior" "[PaaS - Azure Cosmos DB]" "Database"
                centralMonitor = component "Monitoreo Centralizado" "Proporciona supervisión del rendimiento y la disponibilidad para aplicaciones y servicios" "[Software System - SaaS - Azure Monitor]"
                hubsEvent = component "Event Hubs" "Permite ser usado como nodo central de mensajes para la comunicación bidireccional" "[PaaS - Azure Event Hubs]" "Hubs"
                dataMacro = component "MacroDatos" "Servicio de análisis y almacenamiento de datos escalable" "[Software System - Saas - Azure Data Lake]"
                airlineSystem = component "Sistema Existente de la aereolinea" "Permite manejar internamente varios servicios de la empresa, aloja información que esta normada" "[Software System]"
                apiGateway = component "Api Gateway System" "Permite la exposición y consumo de Apis permitiendo la interacción entre empresas del mismo ambito" "[Software System]"
                systemRegulator = component "Software System" "Sistemas Reguladores Normativos, Sistemas de Redes Sociales" "[Software System]"
            }
            roadWarriorAplication = container "Rodar Warrior System" "Permite la interacción directa con los servicios de la aereolinea" "[Container - Azure AKS CaaS - SaaS]"
            centralMonitoring = container "Monitoreo Centralizado" "Proporciona supervisión del rendimiento y la disponibilidad para aplicaciones y servicios" "[Software System - SaaS - Azure Monitor]"
            appOnePageUserExt = container "Aplicaciones de una sola página usuario externo" "Grupo de SPA que permiten la interacción de usuarios no autenticados" "[Angular - MicroFrontEnd]" "Web Browser"{
                apiCampaignUserExt = component "API - BA - CAMPAÑAS" "Permite administrar camapañas de promocionales" "[Spring MVC Rest Controller]"
            }
            eventHubs = container "Event Hubs" "Permite ser usado como nodo central de mensajes para la comunicación bidireccional" "[PaaS - Azure Event Hubs]" "Hubs"
            macroData = container "MacroDatos" "Servicio de análisis y almacenamiento de datos escalable" "[Software System - Saas - Azure Data Lake]"
            cache = container "Caché - Parametrización - Catalogos" "Almacén de estructura de datos de valores de clave en memoria" "[PaaS - Redis Enterprise]" "Database"
            backEnd = container "Contenedor de Orquestaciones - BackEnd" "Permite exponer apis para BFF, para Negocio, para Seguridad, etc" "[Container - Azure AKS CaaS - SaaS - Java SpringBoot]"
            structureData = container "Datos Estructurados" "Aloja la información estructurada proveniente de los formularios de interaccion con el usuario" "[PaaS - Azure SQL Server]" "Database"
            noStructureData = container "Datos no Estructurados" "Aloja la información documental, multimedia y macrodatos que comparte el usuario con Road Warrior" "[PaaS - Azure Cosmos DB]" "Database"
            
        }
        
        apiGatewaySoftwareSystem = softwareSystem "Api Gateway" "Permite la exposición y consumo de Apis permitiendo la interacción entre empresas del mismo ámbito." {
            tags ExistingSystem
        }
        
        /* Begin existingSoftwareSystem */
        
        existingSoftwareSystem = softwareSystem "Sistema existente de la aerolínea Road Warrior" "Permite manejar internamente varios servicios de la empresa, aloja información que esta normada." {
            existingAccessManagement = container "Sistemas de identidad y manejo de accesos" "Permite garantizar la autentificación " "Software System - Azure AD"
           
            reservationsData = container "Reservations Data" "Almacena los datos de las reservas" "[PaaS - Azure SQL Server]" "Database"
            
            webApplicationReservations = container "Web Application Reservations" "Aplicación web que hace uso de componentes de Reservas" "nodejs,express,axios" "Web Browser"{
                reservationsApiAuthentication = component "Autorización sesión usuarios" "Permite administrar la autorización para el uso de componentes de reservas y maneja la sesión de usuario" "API - BA - AUTORIZACION"
                reservationsIdentify = component "Sistema de Identidad y Manejo de Accesos" "Permite garantizar la autentificación y autorización de accesos al sitio" "[Software System - Azure AD]"
                reservationsApiUser = component "API - BA - USUARIOS" "Permite Administrar los usuarios que inician sesión" "[Spring MVC Rest Controller]"
                reservationsApi = component "API - BA - RESERVAS" "Permite administrar reservas" "[Spring MVC Rest Controller]"
                reservationsApiGateway = component "Api Gateway System" "Permite la exposición y consumo de Apis permitiendo la interacción entre empresas del mismo ambito" "[Software System]"
                reservationsDataComponent = component "Reservations Data" "Aloja la información estructurada proveniente de los formularios de interaccion con el usuario internos y reservas" "[PaaS - Azure SQL Server]" "Database"
            }
            
            ratesData = container "Rates Data" "Almacena los datos de las tarifas" "[PaaS - Azure SQL Server]" "Database"    
            
            webApplicationRates = container "Web Application Rates" "Aplicación web que hace uso de componentes de Tarifas" "Vue, axios" "Web Browser"{
                ratesApiAuthentication = component "Autorización sesión usuarios" "Permite administrar la autorización para el uso de componentes de tarifas y maneja la sesión de usuario" "API - BA - AUTORIZACION"
                ratesIdentify = component "Sistema de Identidad y Manejo de Accesos" "Permite garantizar la autentificación y autorización de accesos al sitio" "[Software System - Azure AD]"
                ratesApiUser = component "API - BA - USUARIOS" "Permite Administrar los usuarios que inician sesión" "[Spring MVC Rest Controller]"
                ratesApi = component "API - BA - TARIFAS" "Permite administrar tarifas" "[Spring MVC Rest Controller]"
                ratesApiGateway = component "Api Gateway System" "Permite la exposición y consumo de Apis permitiendo la interacción entre empresas del mismo ambito" "[Software System]"
                ratesDataComponent = component "Rates Data" "Aloja la información estructurada proveniente de los formularios de interaccion con el usuario y tarifas" "[PaaS - Azure SQL Server]" "Database"
            }
        }

        existingSoftwareSystem -> apiGatewaySoftwareSystem "Exponer Apis al exterior"
        apiGatewaySoftwareSystem -> existingSoftwareSystem "Consumir Apis externas"
    
        internalUser -> existingSoftwareSystem "Gestionar costos, gestionar servicios externos [usando]."
        internalUser -> webApplicationReservations "https://ReservationApi.RoadWarrior.com/inicio"
        internalUser -> webApplicationRates "https://RatesApi.RoadWarrior.com/inicio"
        
        webApplicationReservations -> existingAccessManagement "Auth Https-Public/Private"
        webApplicationRates -> existingAccessManagement "Auth Https-Public/Private"

        webApplicationReservations -> reservationsData "CRUD" "[TCP/IP - Private]"
        webApplicationRates -> ratesData "CRUD" "[TCP/IP - Private]"

        reservationsApiAuthentication -> reservationsIdentify "Auth - Redirect Auth" "Https-Public/Private"

        reservationsApiUser -> reservationsIdentify "Auth - Redirect Auth" "Https-Public/Private"
        reservationsApiUser -> reservationsDataComponent "Execute Procedure" "[TCP/IP - Private]"
       
        reservationsApi -> reservationsDataComponent "Llamadas API [Rest: Https - Private]"
        reservationsApi -> reservationsApiGateway "Llamadas API [Rest: Https - Private]"
        reservationsApiGateway -> reservationsApi "Llamadas API [Rest: Https]"
        
        
        ratesApiAuthentication -> ratesIdentify "Auth - Redirect Auth" "Https-Public/Private"

        ratesApiUser -> ratesIdentify "Auth - Redirect Auth" "Https-Public/Private"
        ratesApiUser -> ratesDataComponent "Execute Procedure" "[TCP/IP - Private]"
       
        ratesApi -> ratesDataComponent "Llamadas API [Rest: Https - Private]"
        ratesApi -> ratesApiGateway "Llamadas API [Rest: Https - Private]"
        ratesApiGateway -> ratesApi "Llamadas API [Rest: Https]"

        /* End existingSoftwareSystem */
        
        otherSoftwareSystem = softwareSystem "Sistemas reguladores, normativos, sistemas de redes sociales" "Entidades gubernamentales, FaceBook, Instagram." {
            tags ExistingSystem
        }
        
        /* New system Relationships */
        
        internalUser -> roadWarriorSoftwareSystem "Gestionar reservas, gestionar alojamientos [usando]."
        externalUser -> roadWarriorSoftwareSystem "Cotizar, simular viajes. Promociones de temporada [usando]."
        roadWarriorSoftwareSystem -> existingSoftwareSystem "Datos históricos, reservas, facturación, costos [usando]."
        supplier -> apiGatewaySoftwareSystem "Consumir productos y servicios de la aerolínea [usando]."
        roadWarriorSoftwareSystem -> apiGatewaySoftwareSystem "Consumir Apis externas."
        apiGatewaySoftwareSystem -> roadWarriorSoftwareSystem "Exponer Apis al exterior."
        apiGatewaySoftwareSystem -> otherSoftwareSystem "Facturación, autorización, publicación [usando]."
        
        
        internalUser -> AccessManagement "Auth Https-Public/Private"
        internalUser -> roadWarriorAplication "Visits https://www.RoadWarrior.com/login"
        roadWarriorAplication -> AccessManagement "Redirecciona para Auth [Https-Public/Private]"
        roadWarriorAplication -> appOnePage "Entrega al navegador web del cliente"
        appOnePage -> centralMonitoring "Enviar métricas, trazas, logs [Rest : Https - Public SDK]"
        roadWarriorAplication -> centralMonitoring "Enviar métricas, trazas, logs [Rest : Https - Public SDK]"
        externalUser -> appOnePageUserExt "Visits https://www.RoadWarrior.com/inicio"
        appOnePageUserExt -> centralMonitoring "Enviar métricas, trazas, logs [Rest : Https - Public SDK]"
        roadWarriorAplication -> appOnePageUserExt "Entrega al navegador web del cliente"
        centralMonitoring -> eventHubs "Send Events [Binary Tcp - Private]"
        eventHubs -> macroData "Send Events [Binary Tcp - Private]"
        appOnePage -> cache "Extraer y Cargar Datos en memoria [Redis Pub/Sub]"
        appOnePageUserExt -> cache "Extraer y Cargar Datos en memoria [Redis Pub/Sub]"
        backEnd -> cache "Extraer y Cargar Datos en memoria [Redis Pub/Sub]"
        appOnePage -> backEnd "Llamadas API [Rest: Https - Private]"
        appOnePageUserExt -> backEnd "Llamadas API [Rest: Https - Private]"
        backEnd -> eventHubs "Send Events [Binary Tcp - Private]"
        backEnd -> structureData "Execute Procedure [TCP/IP - Private]"
        backEnd -> noStructureData "Execute Procedure [TCP/IP - Private]"
        backEnd -> existingSoftwareSystem "Llamadas API [Rest: Https - Private]"
        backEnd -> apiGatewaySoftwareSystem "e.g. JSON/HTTP [e.g. JSON/HTTP]"
        existingSoftwareSystem -> apiGatewaySoftwareSystem "e.g. Makes API calls [e.g. JSON/HTTP]"
        supplier -> apiGatewaySoftwareSystem "Llamadas API [Rest: JSON/HTTPS]"
        apiGatewaySoftwareSystem -> otherSoftwareSystem "Llamadas API [Rest: JSON/HTTPS]"
        
        apiAuthentication -> identify "Auth - Redirect Auth" "Https-Public/Private"
        apiAuthentication -> cacheComp "Extraer y Cargar Datos en memoria" "[Redis Pub/Sub]"
        apiAuthentication -> centralMonitor "Enviar métricas, trazas, logs" "[Rest : Https - Public SDK]"
        
        apiUser -> identify "Auth - Redirect Auth" "Https-Public/Private"
        apiUser -> cacheComp "Extraer y Cargar Datos en memoria" "[Redis Pub/Sub]"
        apiUser -> dataStructure "Execute Procedure" "[TCP/IP - Private]"
        apiUser -> dataNoStructure "CRUD" "[TCP/IP - Private]"
        apiUser -> centralMonitor "Enviar métricas, trazas, logs" "[Rest : Https - Public SDK]"
        
        apiCotization -> cacheComp "Extraer y Cargar Datos en memoria" "[Redis Pub/Sub]"
        apiCotization -> centralMonitor "Enviar métricas, trazas, logs" "[Rest : Https - Public SDK]"
        apiCotization -> airlineSystem "Llamadas API [Rest: Https - Private]"
        apiCotization -> apiGateway "Llamadas API [Rest: Https - Private]"
        
        apiReport -> dataStructure "Execute Procedure" "[TCP/IP - Private]"
        apiReport -> dataNoStructure "CRUD" "[TCP/IP - Private]"
        apiReport -> centralMonitor "Enviar métricas, trazas, logs" "[Rest : Https - Public SDK]"
        
        apiCampaign -> dataStructure "Execute Procedure" "[TCP/IP - Private]"
        apiCampaign -> dataNoStructure "CRUD" "[TCP/IP - Private]"
        apiCampaign -> centralMonitor "Enviar métricas, trazas, logs" "[Rest : Https - Public SDK]"
        apiCampaign -> apiGateway "Llamadas API [Rest: Https - Private]"
        
        apiRates -> cacheComp "Extraer y Cargar Datos en memoria" "[Redis Pub/Sub]"
        apiRates -> centralMonitor "Enviar métricas, trazas, logs" "[Rest : Https - Public SDK]"
        apiRates -> airlineSystem "Llamadas API [Rest: Https - Private]"
        apiRates -> apiGateway "Llamadas API [Rest: Https - Private]"
        
        apiContact -> dataNoStructure "CRUD" "[TCP/IP - Private]"
        apiContact -> centralMonitor "Enviar métricas, trazas, logs" "[Rest : Https - Public SDK]"
        
        apiBooking -> cacheComp "Extraer y Cargar Datos en memoria" "[Redis Pub/Sub]"
        apiBooking -> centralMonitor "Enviar métricas, trazas, logs" "[Rest : Https - Public SDK]"
        apiBooking -> airlineSystem "Llamadas API [Rest: Https - Private]"
        apiBooking -> apiGateway "Llamadas API [Rest: Https - Private]"
        
        apiBeneficiary -> centralMonitor "Enviar métricas, trazas, logs" "[Rest : Https - Public SDK]"
        
        centralMonitor -> hubsEvent "Send Events" "[Binary Tcp - Private]"
        hubsEvent -> dataMacro "Send Events" "[Binary Tcp - Private]"
        
        apiGateway -> systemRegulator "Llamadas API [Rest: Https]"
        
    }

    views {
        systemContext roadWarriorSoftwareSystem {
            include roadWarriorSoftwareSystem
            include existingSoftwareSystem
            include apiGatewaySoftwareSystem
            include otherSoftwareSystem
            include internalUser
            include externalUser
            include supplier
            autolayout lr
        }
        
        container roadWarriorSoftwareSystem {
            include *
            include supplier
            autolayout lr
        }
        
        container existingSoftwareSystem {
            include *
            include internalUser
            autolayout lr
        }
        
        component appOnePage "AplicacionesUnaSolaPaginaUsuarioInterno" "Grupo de SPA que permiten la interacción de usuarios autenticados"{
            include *
            autoLayout lr
        }
        
        
        component webApplicationReservations "ReservationApplication" "Api existente para el manejo de informacion de las reservas"{
            include *
            autoLayout lr
        }
        
        component webApplicationRates "RatesApplication" "Api existente para el manejo de informacion de las tarifas"{
            include *
            autoLayout lr
        }
    
        styles {
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "ExistingSystem" {
                background #808080
                color #ffffff
            }
            element "Person" {
                shape person
                background #08427b
                color #ffffff
            }
            element "Web Browser" {
                shape WebBrowser
            }
            element "Database" {
                shape Cylinder
            }
            element "Hubs" {
                shape Pipe
            }
            element "Component" {
                background #85bbf0
                color #000000
            }
        }
        
    }

}